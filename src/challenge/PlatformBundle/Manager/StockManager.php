<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 namespace challenge\PlatformBundle\Manager;

use Doctrine\ORM\EntityManager;
/**
 * Description of StockManager
 *
 * @author Haja-premesu
 */
class StockManager {
    protected $em; //entity manager
    
    function __construct(EntityManager $em) {
       $this->em = $em ;
    }
    public function getRepository(){
        return $this->em->getRepository('challengePlatformBundle:ProductInput');
    }
    public function getStock()
    {
          $connection = $this->em->getConnection();
        $sql =" SELECT
                DISTINCT p.id,  
                (
                IFNULL((SELECT SUM(pi.quantity) FROM product_input PI WHERE pi.product_id = p.id),0) - IFNULL((SELECT SUM(phc.quantity) FROM product_has_command phc WHERE phc.product_id = p.id), 0) - IFNULL((SELECT SUM(po.quantity) FROM product_output po WHERE po.product_id = p.id), 0)
                ) AS stock  
                FROM product p
                ORDER BY p.id ASC
                ";
        $statement = $connection->prepare($sql);
        $statement->execute();
        $result = $statement->fetchAll();
        return $result;
    }
}
