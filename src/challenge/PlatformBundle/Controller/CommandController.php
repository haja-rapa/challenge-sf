<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace challenge\PlatformBundle\Controller;

use challenge\PlatformBundle\Entity\Command;
use challenge\PlatformBundle\Entity\ProductHasCommand;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of CommandeController
 *
 * @author Haja-premesu
 */
class CommandController extends Controller{
    private $em;
    
    public function __construct(ObjectManager $manager) {
        $this->em = $manager;//$this->getDoctrine()->getManager();
    }
    
    /**
     * @Route("/save", name="savetocart")
     */
    public function saveToCartAction(Request $request){
        if ($request->getMethod() == 'POST') {
            $productId = $request->request->get('productId');
            $quantity = $request->request->get('quantity');
            $mycart = $this->get('session')->get('mycart') ? $this->get('session')->get('mycart') : array();
            $mycart[] = array(
                                'productId'=>$productId,
                                'quantity'=>$quantity,
                            );
            $this->get('session')->set('mycart', $mycart);
            $this->addFlash("success", "Le produit est bien ajouté dans votre pannier");
        }
        return $this->redirectToRoute('homepage');
    }
    
    /**
     * @Route("/mycart", name="mycart")
     */
    public function myCartAction(Request $request){
        $mycarts = $this->get('session')->get('mycart');
        $tId = $tQuantity = $tIndex=array();
        if(is_array($mycarts))
        foreach ($mycarts as $key => $value) {
            if(in_array($value['productId'], $tId)){
                $tQuantity[$value['productId']] += $value['quantity'];
            }else{
                $tId[] = $value['productId'];            
                $tQuantity[$value['productId']] = $value['quantity'];
                $tIndex[$value['productId']] = $key;
            }
        }
        $products = $this->em->getRepository("challengePlatformBundle:Product")->findBy(array('id'=>$tId),array('id'=>'ASC'));
        foreach ($products as $key => $value) {
            $value->quantityCart = $tQuantity[$value->getId()];
            $value->indexCart = $tIndex[$value->getId()];
        }
        return $this->render('@challengePlatform/Command/mycart.html.twig', array(
                                                                                'products' => $products,
                                                                                'countCart' => count($mycarts),
                                                                            ));
    }
    /**
     * @Route("/cart/remove", name="cart_remove")
     */
    public function removeAction(Request $request){
        if($request->getMethod() == 'POST'){
            $productId = $request->request->get('productId');
            $mycarts = $this->get('session')->get('mycart');
            foreach ($mycarts as $key => $value) {
                if($value['productId'] == $productId){
                    unset($mycarts[$key]);
                    
                }
            }
            $mycarts = array_values($mycarts);
            $this->get('session')->set('mycart',$mycarts);
            $callbackUrl = $request->request->get('callback_url');
            $this->addFlash("success", "La commande est retirée de votre pannier avec succès.");
            return $this->redirectToRoute($callbackUrl);
        }
        else{
            throw $this->createNotFoundException('Action refusée.');
        }
    }
    /**
     * @Route("/commande/action", name="commande_action")
     */
    public function commandAction(Request $request){
        $mycarts = $this->get('session')->get('mycart');
        $tId = $tQuantity = $tIndex=array();
        foreach ($mycarts as $key => $value) {
            if(in_array($value['productId'], $tId)){
                $tQuantity[$value['productId']] += $value['quantity'];
            }else{
                $tId[] = $value['productId'];            
                $tQuantity[$value['productId']] = $value['quantity'];
                $tIndex[$value['productId']] = $key;
            }
        }
        $products = $this->em->getRepository("challengePlatformBundle:Product")->findBy(array('id'=>$tId),array('id'=>'ASC'));
        
        $oCommand = new Command();
        $user = $this->get('security.token_storage')
                    ->getToken()
                    ->getUser();
        $oCommand->setUser($user);
        $this->em->persist($oCommand);
        foreach ($products as $key => $p) {
            $oProductHasCommand = new ProductHasCommand();
            $oProductHasCommand->setCommand($oCommand);
            $oProductHasCommand->setProduct($p);
            $oProductHasCommand->setQuantity($tQuantity[$p->getId()]);    
            $this->em->persist($oProductHasCommand);
        }
        $this->em->flush();
        $this->addFlash("success", "Votre commande est enregistrée.");
        $this->get('session')->remove('mycart');
        return $this->redirectToRoute('mycommand');
    }
    
    /**
     * @Route("/my-command", name="mycommand")
     */
    public function myCommandAction(Request $request){
        $user = $this->get('security.token_storage')
                    ->getToken()
                    ->getUser();
        $commands = $this->em->getRepository('challengePlatformBundle:Command')->findBy(array('user'=>$user->getId()));
        $countCart = $this->get('session')->get('mycart') ? count($this->get('session')->get('mycart')) : 0;
        return $this->render('@challengePlatform/Command/myCommand.html.twig', array(
                                                                                'commands' => $commands,
                                                                                'countCart' => $countCart,
                                                                            ));
    }
}
