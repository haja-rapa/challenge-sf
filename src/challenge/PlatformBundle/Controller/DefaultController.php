<?php

namespace challenge\PlatformBundle\Controller;

use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    private $em;
    
    public function __construct(ObjectManager $manager) {
        $this->em = $manager;//$this->getDoctrine()->getManager();
    }
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        $stocks = $this->get("platform.stock_manager")->getStock();
        $tId= array();
        $tStock = array();
        foreach ($stocks as $key => $value) {
            $tId[]=$value['id'];
            $tStock[$value['id']]=$value['stock'];
        }
        
        $products = $this->em->getRepository("challengePlatformBundle:Product")->findBy(array('id'=>$tId),array('id'=>'ASC'));
        foreach ($products as $key => $value) {
            $value->stock = $tStock[$value->getId()];
        }
        $countCart = $this->get('session')->get('mycart') ? count($this->get('session')->get('mycart')) : 0;
        return $this->render('@challengePlatform/default/index.html.twig', array(
                                                                                'products' => $products,
                                                                                'countCart' => $countCart,
                                                                            ));
    }
}
