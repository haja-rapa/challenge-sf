<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace challenge\PlatformBundle\Controller;

use challenge\PlatformBundle\Entity\Product;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Description of Product Controller
 *
 * @author Haja-premesu
 */
class ProductController extends Controller{
    private $em;
    
    public function __construct(ObjectManager $manager){
        $this->em = $manager;
    }
    
    /**
     * @Route("/product/detail/{product}", name="show_detail_product")
     */
    public function ShowDetailAction(Product $product){
        if($product){
            $countCart = $this->get('session')->get('mycart') ? count($this->get('session')->get('mycart')) : 0;
            return $this->render('@challengePlatform/Product/detail.html.twig', array(
                                                                                                'product' => $product,
                                                                                                'countCart' => $countCart,
                                                                                            ));
        }
    }
    
}
