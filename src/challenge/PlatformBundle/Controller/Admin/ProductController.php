<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace challenge\PlatformBundle\Controller\Admin;

use challenge\PlatformBundle\Entity\Product;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of ProductController
 *
 * @author Haja-premesu
 */
class ProductController extends Controller{
    private $em;
    
    public function __construct(ObjectManager $manager) {
        $this->em = $manager;//$this->getDoctrine()->getManager();
    }
    
    private function getErrorMessages(Form $form) {
            $errors = array ();
            foreach ( $form->getErrors () as $key => $error ) {
                    if ($form->isRoot ()) {
                            $errors ['#'] [] = $error->getMessage ();
                    } else {
                            $errors [] = $error->getMessage ();
                    }
            }
            foreach ( $form->all () as $child ) {
                    if (! $child->isValid ()) {
                            $errors [$child->getName ()] = $this->getErrorMessages ( $child );
                    }
            }
            return $errors;
    }
    /**
     * @Route("/product/list", name="admin_product_list")
     */
    public function showlistAction(){
        $products = $this->em->getRepository("challengePlatformBundle:Product")->findAll();
        $product = new Product();
        $form = $this->createForm("challenge\PlatformBundle\Form\ProductType", $product);
        return $this->render("@challengePlatform/Admin/Product/showList.html.twig", array(
                                                                                        'products'=>$products,
                                                                                        'menuProduct'=>true,
                                                                                        'menuProductList'=>true,
                                                                                        'form'=>$form->createView(),
                                                                                    ));
    }
    
     /**
     * @Route("/product/new", name="admin_product_new")
     */
    public function newAction(Request $request){
        $product = new Product();
        $form = $this->createForm("challenge\PlatformBundle\Form\ProductType", $product);
         if ($request->getMethod() == 'POST') {
             $form->handleRequest($request);
             if ($form->isValid()) {
                 $this->em->persist($product);
                 $this->em->flush();
                 $this->addFlash("success", "Nouveau produit ajouté avec succès.");
              }
                else {
                  var_dump($this->getErrorMessages($form));die();
                }
        }  
        return $this->redirect($this->generateUrl('admin_product_list')); 
    }
    /**
     * @Route("/product/edit/{product}", name="admin_product_edit")
     */
    public function editAction(Request $request, Product $product){
        if($product){
        $form = $this->createForm("challenge\PlatformBundle\Form\ProductType", $product);
         if ($request->getMethod() == 'POST') {
             $form->handleRequest($request);
             if ($form->isValid()) {
                 $this->em->flush($product);
                 $this->addFlash("success", "Produit modifié avec succès.");
                 return $this->redirectToRoute('admin_product_list');
              }
                else {
                  var_dump($this->getErrorMessages($form));die();
                }
        }  
        return $this->render("@challengePlatform/Admin/Product/edit.html.twig", array(
                                                                                        'product'=>$product,
                                                                                        'menuProduct'=>true,
                                                                                        'menuProductList'=>true,
                                                                                        'form'=>$form->createView(),
                                                                                    ));
        }
        else{
            throw $this->createNotFoundException('Impossible de trouver ce produit.');
        }
    }
    
    /**
     * @Route("/product/remove", name="admin_product_remove")
     */
    public function removeAction(Request $request){
        if($request->getMethod() == 'POST'){
            $productId = $request->request->get('productId');
            $em = $this->getDoctrine()->getManager();
            $oProduct = $this->em->getRepository("challengePlatformBundle:Product")->find($productId);
            $name = $oProduct->getTitle();            
            $this->em->remove($oProduct);
            $this->em->flush();
            $callbackUrl = $request->request->get('callback_url');
            $this->addFlash("success", "Le produit {$name} est supprimé avec succès.");
            return $this->redirectToRoute($callbackUrl);
        }
        else{
            throw $this->createNotFoundException('Action refusée.');
        }
    }
}
