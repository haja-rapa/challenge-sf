<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace challenge\PlatformBundle\Controller\Admin;

use challenge\PlatformBundle\Entity\Type;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of TypeController
 *
 * @author Haja-premesu
 */
class TypeController extends Controller{
    private $em;
    
    public function __construct(ObjectManager $manager) {
        $this->em = $manager;//$this->getDoctrine()->getManager();
    }
    
    private function getErrorMessages(Form $form) {
            $errors = array ();
            foreach ( $form->getErrors () as $key => $error ) {
                    if ($form->isRoot ()) {
                            $errors ['#'] [] = $error->getMessage ();
                    } else {
                            $errors [] = $error->getMessage ();
                    }
            }
            foreach ( $form->all () as $child ) {
                    if (! $child->isValid ()) {
                            $errors [$child->getName ()] = $this->getErrorMessages ( $child );
                    }
            }
            return $errors;
    }
    /**
     * @Route("/type/list", name="admin_type_list")
     */
    public function showlistAction(){
        $types = $this->em->getRepository("challengePlatformBundle:Type")->findAll();
        $type = new Type();
        $form = $this->createForm("challenge\PlatformBundle\Form\TypeType", $type);
        return $this->render("@challengePlatform/Admin/Type/showList.html.twig", array(
                                                                                        'types'=>$types,
                                                                                        'menuProduct'=>true,
                                                                                        'menuType'=>true,
                                                                                        'form'=>$form->createView(),
                                                                                    ));
    }
    
     /**
     * @Route("/type/new", name="admin_type_new")
     */
    public function newAction(Request $request){
        $type = new Type();
        $form = $this->createForm("challenge\PlatformBundle\Form\TypeType", $type);
         if ($request->getMethod() == 'POST') {
             $form->handleRequest($request);
             if ($form->isValid()) {
                 $this->em->persist($type);
                 $this->em->flush();
                 $this->addFlash("success", "Nouveau type ajouté avec succès.");
              }
                else {
                  var_dump($this->getErrorMessages($form));die();
                }
        }  
        return $this->redirect($this->generateUrl('admin_type_list')); 
    }
    /**
     * @Route("/type/edit/{type}", name="admin_type_edit")
     */
    public function editAction(Request $request, Type $type){
        if($type){
        $form = $this->createForm("challenge\PlatformBundle\Form\TypeType", $type);
         if ($request->getMethod() == 'POST') {
             $form->handleRequest($request);
             if ($form->isValid()) {
                 $this->em->flush($type);
                 $this->addFlash("success", "Type modifié avec succès.");
                 return $this->redirectToRoute('admin_type_list');
              }
                else {
                  var_dump($this->getErrorMessages($form));die();
                }
        }  
        return $this->render("@challengePlatform/Admin/Type/edit.html.twig", array(
                                                                                        'type'=>$type,
                                                                                        'menuProduct'=>true,
                                                                                        'menuType'=>true,
                                                                                        'form'=>$form->createView(),
                                                                                    ));
        }
        else{
            throw $this->createNotFoundException('Impossible de trouver ce type.');
        }
    }
    
    /**
     * @Route("/type/remove", name="admin_type_remove")
     */
    public function removeAction(Request $request){
        if($request->getMethod() == 'POST'){
            $typeId = $request->request->get('typeId');
            $em = $this->getDoctrine()->getManager();
            $oType = $this->em->getRepository("challengePlatformBundle:Type")->find($typeId);
            $name = $oType->getName();            
            $this->em->remove($oType);
            $this->em->flush();
            $callbackUrl = $request->request->get('callback_url');
            $this->addFlash("success", "Le type {$name} est supprimé avec succès.");
            return $this->redirectToRoute($callbackUrl);
        }
        else{
            throw $this->createNotFoundException('Action refusée.');
        }
    }
}
