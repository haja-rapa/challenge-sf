<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace challenge\PlatformBundle\Controller\Admin;

use challenge\PlatformBundle\Entity\Fournisseur;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of FournisseurController
 *
 * @author Haja-premesu
 */
class FournisseurController extends Controller{
    private $em;
    
    public function __construct(ObjectManager $manager) {
        $this->em = $manager;//$this->getDoctrine()->getManager();
    }
    
    private function getErrorMessages(Form $form) {
            $errors = array ();
            foreach ( $form->getErrors () as $key => $error ) {
                    if ($form->isRoot ()) {
                            $errors ['#'] [] = $error->getMessage ();
                    } else {
                            $errors [] = $error->getMessage ();
                    }
            }
            foreach ( $form->all () as $child ) {
                    if (! $child->isValid ()) {
                            $errors [$child->getName ()] = $this->getErrorMessages ( $child );
                    }
            }
            return $errors;
    }
    /**
     * @Route("/fournisseur/list", name="admin_fournisseur_list")
     */
    public function showlistAction(){
        $fournisseurs = $this->em->getRepository("challengePlatformBundle:Fournisseur")->findAll();
        $fournisseur = new Fournisseur();
        $form = $this->createForm("challenge\PlatformBundle\Form\FournisseurType", $fournisseur);
        return $this->render("@challengePlatform/Admin/Fournisseur/showList.html.twig", array(
                                                                                        'fournisseurs'=>$fournisseurs,
                                                                                        'menuProduct'=>true,
                                                                                        'menuFournisseur'=>true,
                                                                                        'form'=>$form->createView(),
                                                                                    ));
    }
    
     /**
     * @Route("/fournisseur/new", name="admin_fournisseur_new")
     */
    public function newAction(Request $request){
        $fournisseur = new Fournisseur();
        $form = $this->createForm("challenge\PlatformBundle\Form\FournisseurType", $fournisseur);
         if ($request->getMethod() == 'POST') {
             $form->handleRequest($request);
             if ($form->isValid()) {
                 $this->em->persist($fournisseur);
                 $this->em->flush();
                 $this->addFlash("success", "Nouveau fournisseur ajouté avec succès.");
              }
                else {
                  var_dump($this->getErrorMessages($form));die();
                }
        }  
        return $this->redirect($this->generateUrl('admin_fournisseur_list')); 
    }
    /**
     * @Route("/fournisseur/edit/{fournisseur}", name="admin_fournisseur_edit")
     */
    public function editAction(Request $request, Fournisseur $fournisseur){
        if($fournisseur){
        $form = $this->createForm("challenge\PlatformBundle\Form\FournisseurType", $fournisseur);
         if ($request->getMethod() == 'POST') {
             $form->handleRequest($request);
             if ($form->isValid()) {
                 $this->em->flush($fournisseur);
                 $this->addFlash("success", "Fournisseur modifié avec succès.");
                 return $this->redirectToRoute('admin_fournisseur_list');
              }
                else {
                  var_dump($this->getErrorMessages($form));die();
                }
        }  
        return $this->render("@challengePlatform/Admin/Fournisseur/edit.html.twig", array(
                                                                                        'fournisseur'=>$fournisseur,
                                                                                        'menuProduct'=>true,
                                                                                        'menuFournisseur'=>true,
                                                                                        'form'=>$form->createView(),
                                                                                    ));
        }
        else{
            throw $this->createNotFoundException('Impossible de trouver ce fournisseur.');
        }
    }
    
    /**
     * @Route("/fournisseur/remove", name="admin_fournisseur_remove")
     */
    public function removeAction(Request $request){
        if($request->getMethod() == 'POST'){
            $fournisseurId = $request->request->get('fournisseurId');
            $em = $this->getDoctrine()->getManager();
            $oFournisseur = $this->em->getRepository("challengePlatformBundle:Fournisseur")->find($fournisseurId);
            $name = $oFournisseur->getName();            
            $this->em->remove($oFournisseur);
            $this->em->flush();
            $callbackUrl = $request->request->get('callback_url');
            $this->addFlash("success", "Le fournisseur {$name} est supprimé avec succès.");
            return $this->redirectToRoute($callbackUrl);
        }
        else{
            throw $this->createNotFoundException('Action refusée.');
        }
    }
}
