<?php
namespace challenge\PlatformBundle\Controller\Admin;

use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DashboardController
 *
 * @author Haja-premesu
 */
class DashboardController extends Controller{
     private $em;
    
    public function __construct(ObjectManager $manager) {
        $this->em = $manager;//$this->getDoctrine()->getManager();
    }

    /**
     * @Route("/dashboard", name="admin_dashboard")
     */
    public function dashboardAction(){
        $countUsers = count($this->em->getRepository("challengeUserBundle:User")->findAll());
        $countProducts = count($this->em->getRepository("challengePlatformBundle:Product")->findAll());
        $countCommands = count($this->em->getRepository("challengePlatformBundle:Command")->findAll());
        return $this->render('@challengePlatform/Admin/dashboard.html.twig',array(
                                                                                  'countUsers' => $countUsers,
                                                                                  'countProducts' => $countProducts,
                                                                                  'countCommands' => $countCommands,
                                                                                    ));
    }
    /**
     * @Route("/", name="admin_index")
     */
    public function indexAction(){
        return $this->dashboardAction();
    }
}
