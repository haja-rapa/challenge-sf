<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace challenge\PlatformBundle\Controller\Admin;

use challenge\PlatformBundle\Entity\Product;
use challenge\PlatformBundle\Entity\ProductInput;
use challenge\PlatformBundle\Entity\ProductOutput;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of StockController
 *
 * @author Haja-premesu
 */
class StockController extends Controller{
     private $em;
    
    public function __construct(ObjectManager $manager) {
        $this->em = $manager;//$this->getDoctrine()->getManager();
    }
    
    private function getErrorMessages(Form $form) {
            $errors = array ();
            foreach ( $form->getErrors () as $key => $error ) {
                    if ($form->isRoot ()) {
                            $errors ['#'] [] = $error->getMessage ();
                    } else {
                            $errors [] = $error->getMessage ();
                    }
            }
            foreach ( $form->all () as $child ) {
                    if (! $child->isValid ()) {
                            $errors [$child->getName ()] = $this->getErrorMessages ( $child );
                    }
            }
            return $errors;
    }
    /**
     * @Route("/stock/list", name="admin_stock_list")
     */
    public function showlistAction(){
        $stocks = $this->get("platform.stock_manager")->getStock();
        $tId= array();
        $tStock = array();
        foreach ($stocks as $key => $value) {
            $tId[]=$value['id'];
            $tStock[$value['id']]=$value['stock'];
        }
        
        $products = $this->em->getRepository("challengePlatformBundle:Product")->findBy(array('id'=>$tId),array('id'=>'ASC'));
        foreach ($products as $key => $value) {
            $value->stock = $tStock[$value->getId()];
        }
        $productInput = new ProductInput();
        $form = $this->createForm("challenge\PlatformBundle\Form\ProductInputType", $productInput);
        $productOutput = new ProductOutput();
        $formOutput = $this->createForm("challenge\PlatformBundle\Form\ProductOutputType", $productOutput);
        return $this->render("@challengePlatform/Admin/Stock/showList.html.twig", array(
                                                                                        'products'=>$products,
                                                                                        'menuStock'=>true,
                                                                                        'form'=>$form->createView(),
                                                                                        'formOutput'=>$formOutput->createView(),
                                                                                    ));
    }
    
     /**
     * @Route("/stock/new", name="admin_product_input_new")
     */
    public function newInputAction(Request $request){
        $productInput = new ProductInput();
        $form = $this->createForm("challenge\PlatformBundle\Form\ProductInputType", $productInput);
         if ($request->getMethod() == 'POST') {
             $form->handleRequest($request);
             if ($form->isValid()) {
                 $this->em->persist($productInput);
                 $this->em->flush();
                 $this->addFlash("success", "Nouvelle entrée du produit {$productInput->getProduct()->getTitle()}  ajoutée avec succès.");
              }
                else {
                  var_dump($this->getErrorMessages($form));die('error validation input');
                }
        }  
        return $this->redirect($this->generateUrl('admin_stock_list')); 
    }
    /**
     * @Route("/stock/retrait", name="admin_product_output_new")
     */
    public function newOutputAction(Request $request){
        $productOutput = new ProductOutput();
        $form = $this->createForm("challenge\PlatformBundle\Form\ProductOutputType", $productOutput);
         if ($request->getMethod() == 'POST') {
             $form->handleRequest($request);
             if ($form->isValid()) {
                 $this->em->persist($productOutput);
                 $this->em->flush();
                 $this->addFlash("success", "Retrait du produit {$productOutput->getProduct()->getTitle()}  faite avec succès.");
              }
                else {
                  var_dump($this->getErrorMessages($form));die('error validation output');
                }
        }  
        return $this->redirect($this->generateUrl('admin_stock_list')); 
    }
}
