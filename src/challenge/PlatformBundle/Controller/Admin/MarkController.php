<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace challenge\PlatformBundle\Controller\Admin;

use challenge\PlatformBundle\Entity\Mark;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of MarkController
 *
 * @author Haja-premesu
 */
class MarkController extends Controller {
    private $em;
    
    public function __construct(ObjectManager $manager) {
        $this->em = $manager;//$this->getDoctrine()->getManager();
    }
    
    private function getErrorMessages(Form $form) {
            $errors = array ();
            foreach ( $form->getErrors () as $key => $error ) {
                    if ($form->isRoot ()) {
                            $errors ['#'] [] = $error->getMessage ();
                    } else {
                            $errors [] = $error->getMessage ();
                    }
            }
            foreach ( $form->all () as $child ) {
                    if (! $child->isValid ()) {
                            $errors [$child->getName ()] = $this->getErrorMessages ( $child );
                    }
            }
            return $errors;
    }
    /**
     * @Route("/mark/list", name="admin_mark_list")
     */
    public function showlistAction(){
        $marks = $this->em->getRepository("challengePlatformBundle:Mark")->findAll();
        $mark = new Mark();
        $form = $this->createForm("challenge\PlatformBundle\Form\MarkType", $mark);
        return $this->render("@challengePlatform/Admin/Mark/showList.html.twig", array(
                                                                                        'marks'=>$marks,
                                                                                        'menuProduct'=>true,
                                                                                        'menuMark'=>true,
                                                                                        'form'=>$form->createView(),
                                                                                    ));
    }
    
     /**
     * @Route("/mark/new", name="admin_mark_new")
     */
    public function newAction(Request $request){
        $mark = new Mark();
        $form = $this->createForm("challenge\PlatformBundle\Form\MarkType", $mark);
         if ($request->getMethod() == 'POST') {
             $form->handleRequest($request);
             if ($form->isValid()) {
                 $this->em->persist($mark);
                 $this->em->flush();
                 $this->addFlash("success", "Nouveau mark ajouté avec succès.");
              }
                else {
                  var_dump($this->getErrorMessages($form));die();
                }
        }  
        return $this->redirect($this->generateUrl('admin_mark_list')); 
    }
    /**
     * @Route("/mark/edit/{mark}", name="admin_mark_edit")
     */
    public function editAction(Request $request, Mark $mark){
        if($mark){
        $form = $this->createForm("challenge\PlatformBundle\Form\MarkType", $mark);
         if ($request->getMethod() == 'POST') {
             $form->handleRequest($request);
             if ($form->isValid()) {
                 $this->em->flush($mark);
                 $this->addFlash("success", "Mark modifié avec succès.");
                 return $this->redirectToRoute('admin_mark_list');
              }
                else {
                  var_dump($this->getErrorMessages($form));die();
                }
        }  
        return $this->render("@challengePlatform/Admin/Mark/edit.html.twig", array(
                                                                                        'mark'=>$mark,
                                                                                        'menuProduct'=>true,
                                                                                        'menuMark'=>true,
                                                                                        'form'=>$form->createView(),
                                                                                    ));
        }
        else{
            throw $this->createNotFoundException('Impossible de trouver ce mark.');
        }
    }
    
    /**
     * @Route("/mark/remove", name="admin_mark_remove")
     */
    public function removeAction(Request $request){
        if($request->getMethod() == 'POST'){
            $markId = $request->request->get('markId');
            $em = $this->getDoctrine()->getManager();
            $oMark = $this->em->getRepository("challengePlatformBundle:Mark")->find($markId);
            $name = $oMark->getName();            
            $this->em->remove($oMark);
            $this->em->flush();
            $callbackUrl = $request->request->get('callback_url');
            $this->addFlash("success", "Le mark {$name} est supprimé avec succès.");
            return $this->redirectToRoute($callbackUrl);
        }
        else{
            throw $this->createNotFoundException('Action refusée.');
        }
    }
}
