<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace challenge\PlatformBundle\Controller\Admin;

use challenge\PlatformBundle\Entity\Genre;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of GenreController
 *
 * @author Haja-premesu
 */
class GenreController extends Controller{
    private $em;
    
    public function __construct(ObjectManager $manager) {
        $this->em = $manager;//$this->getDoctrine()->getManager();
    }
    
    private function getErrorMessages(Form $form) {
            $errors = array ();
            foreach ( $form->getErrors () as $key => $error ) {
                    if ($form->isRoot ()) {
                            $errors ['#'] [] = $error->getMessage ();
                    } else {
                            $errors [] = $error->getMessage ();
                    }
            }
            foreach ( $form->all () as $child ) {
                    if (! $child->isValid ()) {
                            $errors [$child->getName ()] = $this->getErrorMessages ( $child );
                    }
            }
            return $errors;
    }
    /**
     * @Route("/genre/list", name="admin_genre_list")
     */
    public function showlistAction(){
        $genres = $this->em->getRepository("challengePlatformBundle:Genre")->findAll();
        $genre = new Genre();
        $form = $this->createForm("challenge\PlatformBundle\Form\GenreType", $genre);
        return $this->render("@challengePlatform/Admin/Genre/showList.html.twig", array(
                                                                                        'genres'=>$genres,
                                                                                        'menuProduct'=>true,
                                                                                        'menuGenre'=>true,
                                                                                        'form'=>$form->createView(),
                                                                                    ));
    }
    
     /**
     * @Route("/genre/new", name="admin_genre_new")
     */
    public function newAction(Request $request){
        $genre = new Genre();
        $form = $this->createForm("challenge\PlatformBundle\Form\GenreType", $genre);
         if ($request->getMethod() == 'POST') {
             $form->handleRequest($request);
             if ($form->isValid()) {
                 $this->em->persist($genre);
                 $this->em->flush();
                 $this->addFlash("success", "Nouveau genre ajouté avec succès.");
              }
                else {
                  var_dump($this->getErrorMessages($form));die();
                }
        }  
        return $this->redirect($this->generateUrl('admin_genre_list')); 
    }
    /**
     * @Route("/genre/edit/{genre}", name="admin_genre_edit")
     */
    public function editAction(Request $request, Genre $genre){
        if($genre){
        $form = $this->createForm("challenge\PlatformBundle\Form\GenreType", $genre);
         if ($request->getMethod() == 'POST') {
             $form->handleRequest($request);
             if ($form->isValid()) {
                 $this->em->flush($genre);
                 $this->addFlash("success", "Genre modifié avec succès.");
                 return $this->redirectToRoute('admin_genre_list');
              }
                else {
                  var_dump($this->getErrorMessages($form));die();
                }
        }  
        return $this->render("@challengePlatform/Admin/Genre/edit.html.twig", array(
                                                                                        'genre'=>$genre,
                                                                                        'menuProduct'=>true,
                                                                                        'menuGenre'=>true,
                                                                                        'form'=>$form->createView(),
                                                                                    ));
        }
        else{
            throw $this->createNotFoundException('Impossible de trouver ce genre.');
        }
    }
    
    /**
     * @Route("/genre/remove", name="admin_genre_remove")
     */
    public function removeAction(Request $request){
        if($request->getMethod() == 'POST'){
            $genreId = $request->request->get('genreId');
            $em = $this->getDoctrine()->getManager();
            $oGenre = $this->em->getRepository("challengePlatformBundle:Genre")->find($genreId);
            $name = $oGenre->getName();            
            $this->em->remove($oGenre);
            $this->em->flush();
            $callbackUrl = $request->request->get('callback_url');
            $this->addFlash("success", "Le genre {$name} est supprimé avec succès.");
            return $this->redirectToRoute($callbackUrl);
        }
        else{
            throw $this->createNotFoundException('Action refusée.');
        }
    }
}
