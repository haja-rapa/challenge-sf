<?php

namespace challenge\PlatformBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Command
 *
 * @ORM\Table(name="command")
 * @ORM\Entity(repositoryClass="challenge\PlatformBundle\Repository\CommandRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Command
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetimetz")
     */
    private $date;
    
    /**
    * @ORM\ManyToOne(targetEntity="challenge\UserBundle\Entity\User" , inversedBy="commands")
    * @ORM\JoinColumn(nullable=false)
    */
    private $user;
    
    /**
    * @ORM\OneToMany(targetEntity="challenge\PlatformBundle\Entity\ProductHasCommand" , mappedBy="command", orphanRemoval=true)
    */
    private $productHasCommand;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Command
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set user
     *
     * @param \challenge\UserBundle\Entity\User $user
     *
     * @return Command
     */
    public function setUser(\challenge\UserBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \challenge\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
    /**
     * Triggered on insert
     * @ORM\PrePersist
     */
    public function onPrePersist(){
        $this->setDate(new \DateTime);
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->productHasCommand = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add productHasCommand
     *
     * @param \challenge\PlatformBundle\Entity\ProductHasCommand $productHasCommand
     *
     * @return Command
     */
    public function addProductHasCommand(\challenge\PlatformBundle\Entity\ProductHasCommand $productHasCommand)
    {
        $this->productHasCommand[] = $productHasCommand;

        return $this;
    }

    /**
     * Remove productHasCommand
     *
     * @param \challenge\PlatformBundle\Entity\ProductHasCommand $productHasCommand
     */
    public function removeProductHasCommand(\challenge\PlatformBundle\Entity\ProductHasCommand $productHasCommand)
    {
        $this->productHasCommand->removeElement($productHasCommand);
    }

    /**
     * Get productHasCommand
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductHasCommand()
    {
        return $this->productHasCommand;
    }
}
