<?php

namespace challenge\PlatformBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Product
 *
 * @ORM\Table(name="product")
 * @ORM\Entity(repositoryClass="challenge\PlatformBundle\Repository\ProductRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Product
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=500)
     */
    private $description;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float")
     */
    private $price;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetimetz")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedAt", type="datetimetz", nullable=true)
     */
    private $updatedAt;

    /**
    * @ORM\ManyToOne(targetEntity="challenge\PlatformBundle\Entity\Mark")
    * @ORM\JoinColumn(nullable=false)
    */
    private $mark;
    
    /**
    * @ORM\ManyToOne(targetEntity="challenge\PlatformBundle\Entity\Type")
    * @ORM\JoinColumn(nullable=false)
    */
    private $type;
    
    /**
    * @ORM\ManyToOne(targetEntity="challenge\PlatformBundle\Entity\Genre")
    * @ORM\JoinColumn(nullable=false)
    */
    private $genre;
    
    /**
    * @ORM\OneToMany(targetEntity="challenge\PlatformBundle\Entity\ProductHasCommand" , mappedBy="product", orphanRemoval=true)
    * @ORM\JoinColumn(nullable=false)
    */
    private $productHasCommand;
    
    /**
    * @ORM\OneToMany(targetEntity="challenge\PlatformBundle\Entity\ProductInput" , mappedBy="product", orphanRemoval=true)
    * @ORM\JoinColumn(nullable=false)
    */
    private $productInput;
    
    /**
    * @ORM\OneToMany(targetEntity="challenge\PlatformBundle\Entity\ProductOutput" , mappedBy="product", orphanRemoval=true)
    * @ORM\JoinColumn(nullable=false)
    */
    private $ProductOutput;
    
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Product
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Product
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return Product
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Product
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Product
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set mark
     *
     * @param \challenge\PlatformBundle\Entity\Mark $mark
     *
     * @return Product
     */
    public function setMark(\challenge\PlatformBundle\Entity\Mark $mark)
    {
        $this->mark = $mark;

        return $this;
    }

    /**
     * Get mark
     *
     * @return \challenge\PlatformBundle\Entity\Mark
     */
    public function getMark()
    {
        return $this->mark;
    }

    /**
     * Set type
     *
     * @param \challenge\PlatformBundle\Entity\Type $type
     *
     * @return Product
     */
    public function setType(\challenge\PlatformBundle\Entity\Type $type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \challenge\PlatformBundle\Entity\Type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set genre
     *
     * @param \challenge\PlatformBundle\Entity\Genre $genre
     *
     * @return Product
     */
    public function setGenre(\challenge\PlatformBundle\Entity\Genre $genre)
    {
        $this->genre = $genre;

        return $this;
    }

    /**
     * Get genre
     *
     * @return \challenge\PlatformBundle\Entity\Genre
     */
    public function getGenre()
    {
        return $this->genre;
    }
    
    /**
     * Triggered on insert
     * @ORM\PrePersist
     */
    public function onPrePersist(){
        $this->setCreatedAt(new \DateTime);
    }
    /**
     * Triggered on update
     * @ORM\PreUpdate
     */
    public function onPreUpdate(){
        $this->setUpdatedAt(new \DateTime);
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->productHasCommand = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add productHasCommand
     *
     * @param \challenge\PlatformBundle\Entity\ProductHasCommand $productHasCommand
     *
     * @return Product
     */
    public function addProductHasCommand(\challenge\PlatformBundle\Entity\ProductHasCommand $productHasCommand)
    {
        $this->productHasCommand[] = $productHasCommand;

        return $this;
    }

    /**
     * Remove productHasCommand
     *
     * @param \challenge\PlatformBundle\Entity\ProductHasCommand $productHasCommand
     */
    public function removeProductHasCommand(\challenge\PlatformBundle\Entity\ProductHasCommand $productHasCommand)
    {
        $this->productHasCommand->removeElement($productHasCommand);
    }

    /**
     * Get productHasCommand
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductHasCommand()
    {
        return $this->productHasCommand;
    }
}
