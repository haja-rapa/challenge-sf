<?php

namespace challenge\PlatformBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductHasCommand
 *
 * @ORM\Table(name="product_has_command")
 * @ORM\Entity(repositoryClass="challenge\PlatformBundle\Repository\ProductHasCommandRepository")
 */
class ProductHasCommand
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="quantity", type="integer")
     */
    private $quantity;
    
    /**
    * @ORM\ManyToOne(targetEntity="challenge\PlatformBundle\Entity\Product", inversedBy="productHasCommand")
    * @ORM\JoinColumn(nullable=false)
    */
    private $product;
    
    /**
    * @ORM\ManyToOne(targetEntity="challenge\PlatformBundle\Entity\Command" , inversedBy="productHasCommand")
    * @ORM\JoinColumn(nullable=false)
    */
    private $command;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     *
     * @return ProductHasCommand
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set product
     *
     * @param \challenge\PlatformBundle\Entity\Product $product
     *
     * @return ProductHasCommand
     */
    public function setProduct(\challenge\PlatformBundle\Entity\Product $product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \challenge\PlatformBundle\Entity\Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set command
     *
     * @param \challenge\PlatformBundle\Entity\Command $command
     *
     * @return ProductHasCommand
     */
    public function setCommand(\challenge\PlatformBundle\Entity\Command $command)
    {
        $this->command = $command;

        return $this;
    }

    /**
     * Get command
     *
     * @return \challenge\PlatformBundle\Entity\Command
     */
    public function getCommand()
    {
        return $this->command;
    }
}
