<?php

namespace challenge\PlatformBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductOutput
 *
 * @ORM\Table(name="product_output")
 * @ORM\Entity(repositoryClass="challenge\PlatformBundle\Repository\ProductOutputRepository")
 * @ORM\HasLifecycleCallbacks
 */
class ProductOutput
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="motif", type="string", length=255, nullable=true)
     */
    private $motif;

    /**
     * @var int
     *
     * @ORM\Column(name="quantity", type="integer")
     */
    private $quantity;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetimetz")
     */
    private $date;

     /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetimetz")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedAt", type="datetimetz", nullable=true)
     */
    private $updatedAt;
    
    /**
    * @ORM\ManyToOne(targetEntity="challenge\PlatformBundle\Entity\Product" , inversedBy="productOutput")
    * @ORM\JoinColumn(nullable=false)
    */
    private $product;
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set motif
     *
     * @param string $motif
     *
     * @return ProductOutput
     */
    public function setMotif($motif)
    {
        $this->motif = $motif;

        return $this;
    }

    /**
     * Get motif
     *
     * @return string
     */
    public function getMotif()
    {
        return $this->motif;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     *
     * @return ProductOutput
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return ProductOutput
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return ProductOutput
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return ProductOutput
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set product
     *
     * @param \challenge\PlatformBundle\Entity\Product $product
     *
     * @return ProductOutput
     */
    public function setProduct(\challenge\PlatformBundle\Entity\Product $product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \challenge\PlatformBundle\Entity\Product
     */
    public function getProduct()
    {
        return $this->product;
    }
    /**
     * Triggered on insert
     * @ORM\PrePersist
     */
    public function onPrePersist(){
        $this->setCreatedAt(new \DateTime);
        $this->setDate(new \DateTime);
    }
    /**
     * Triggered on update
     * @ORM\PreUpdate
     */
    public function onPreUpdate(){
        $this->setUpdatedAt(new \DateTime);
    }
}
