<?php

namespace challenge\PlatformBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MarkType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name',TextType::class,array(
                                           'label'=>"Nom : ",
                                                        ))
                ->add('fournisseur', EntityType::class, array(
                                            'class' => 'challengePlatformBundle:Fournisseur',
                                            'query_builder' => function (EntityRepository $er) {
                                                return $er->createQueryBuilder('f')
                                                          ->leftJoin('f.mark','m')
                                                          ->where('m.id IS NULL')
                                                          ->orderBy('f.name', 'ASC');
                                            },
                                            'choice_label' => 'name',
                                            'label'=>"Fournisseur: ",                                
                                                ))
                                                    ;
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'challenge\PlatformBundle\Entity\Mark'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'challenge_platformbundle_mark';
    }


}
