<?php

namespace challenge\PlatformBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title',TextType::class,array(
                                           'label'=>"Titre: ",
                                                        ))
                ->add('description', TextType::class,array(
                                           'label'=>"Description: ",
                                                        ))
                ->add('price', MoneyType::class,array(
                                           'label'=>"Prix: ",
                                                        ))               

                ->add('type', EntityType::class, array(
                                            'class' => 'challengePlatformBundle:Type',
                                            'query_builder' => function (EntityRepository $er) {
                                                return $er->createQueryBuilder('t')
                                                    ->orderBy('t.name', 'ASC');
                                            },
                                            'choice_label' => 'name',
                                            'label'=>"Type: ",                                
                                                ))
                ->add('genre', EntityType::class, array(
                                            'class' => 'challengePlatformBundle:Genre',
                                            'query_builder' => function (EntityRepository $er) {
                                                return $er->createQueryBuilder('g')
                                                    ->orderBy('g.name', 'ASC');
                                            },
                                            'choice_label' => 'name',
                                            'label'=>"Genre: ",                                
                                                ))
                ->add('mark', EntityType::class, array(
                                            'class' => 'challengePlatformBundle:Mark',
                                            'query_builder' => function (EntityRepository $er) {
                                                return $er->createQueryBuilder('m')
                                                    ->orderBy('m.name', 'ASC');
                                            },
                                            'choice_label' => 'name',
                                            'label'=>"Marque: ",                                
                                                ))
                ;
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'challenge\PlatformBundle\Entity\Product'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'challenge_platformbundle_product';
    }


}
