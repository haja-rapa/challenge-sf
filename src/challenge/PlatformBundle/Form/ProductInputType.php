<?php

namespace challenge\PlatformBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductInputType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('quantity', IntegerType::class,array(
                                                    'label'=> 'Quantité: '
                                                ))
                ->add('product', EntityType::class, array(
                                            'class' => 'challengePlatformBundle:Product',
                                            'query_builder' => function (EntityRepository $er) {
                                                return $er->createQueryBuilder('p')
                                                    ->orderBy('p.title', 'ASC');
                                            },
                                            'choice_label' => 'title',
                                            'label'=>"Produit: ",                                
                                                ))
                                                    ;
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'challenge\PlatformBundle\Entity\ProductInput'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'challenge_platformbundle_productinput';
    }


}
