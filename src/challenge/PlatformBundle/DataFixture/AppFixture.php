<?php
namespace challenge\PlatformBundle\DataFixture;

use challenge\PlatformBundle\Entity\Fournisseur;
use challenge\PlatformBundle\Entity\Genre;
use challenge\PlatformBundle\Entity\Type;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AppFixture
 *
 * @author Haja-premesu
 */
class AppFixture extends Fixture{
    
    public function load(ObjectManager $manager) {
        // create Default Type! 
        $arrayType = ['soleil', 'vue', 'sport'];
        foreach ($arrayType as $value) {
            $oType = new Type();
            $oType->setName($value);
            $manager->persist($oType);
        }
        
        // create Default Genre! 
        $arrayGenre = ['homme', 'femme', 'mixte', 'enfant'];
        foreach ($arrayGenre as $value) {
            $oGenre = new Genre();
            $oGenre->setName($value);
            $manager->persist($oGenre);
        }
        // create Default Fournisseur! 
        $arrayFournisseur = ['fournisseur 1', 'fournisseur 2'];
        foreach ($arrayFournisseur as $value) {
            $oFournisseur = new Fournisseur();
            $oFournisseur->setName($value);
            $manager->persist($oFournisseur);
        }
        
        $manager->flush();
    }

//put your code here
}
