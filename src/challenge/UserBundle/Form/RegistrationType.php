<?php

namespace challenge\UserBundle\Form;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of RegistrationType
 *
 * @author Haja-premesu
 */

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegistrationType extends AbstractType
{
    /**
     * @var string
     */
    private $class = 'challenge\UserBundle\Entity\User';

    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('email', EmailType::class, array(
                                                    'label' => 'Email',
                                                    'translation_domain' => 'FOSUserBundle'
                                                        ))
                ->add('firstname',TextType::class,array(
                                           'label'=>"Prénom: ",
                                                        ))
                ->add('lastname',TextType::class,array(
                                           'label'=>"Nom: ",
                                                        ))
                ->add('address',TextType::class,array(
                                           'label'=>"Adresse: ",
                                                        ))
                ->add('phone',TextType::class,array(
                                           'label'=>"Téléphone: ",
                                                        ))        
                
                ->add('username', HiddenType::class)
                ->add('plainPassword', RepeatedType::class, array(
                    'type' => PasswordType::class,
                    'options' => array(
                        'translation_domain' => 'FOSUserBundle',
                        'attr' => array(
                            'autocomplete' => 'new-password',
                        ),
                    ),
                    'first_options' => array('label' => 'mot de passe'),
                    'second_options' => array('label' => 'confirmation du mot de passe'),
                    'invalid_message' => 'mot de passe non conforme',
                ))
                ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
                        $user = $event->getData();
                        $form = $event->getForm();
                        if (!$user) {
                            return;
                        }
                        $user['username'] = $user['email'];        
                        $event->setData($user);
                    })
                    ->getForm()
            ;
        }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => $this->class,
            'csrf_token_id' => 'registration',
        ));
    }

    public function getBlockPrefix()
    {
        return 'app_user_registration';
    }

    
}
