<?php

namespace challenge\UserBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Security;

class SecurityController extends Controller
{
    const AUTHENTICATION_ERROR = Security::AUTHENTICATION_ERROR;
    const LAST_USERNAME = Security::LAST_USERNAME;
         
    /**
     * @Route("/admin/login", name="admin_login")
     * @param Request $request
     * @return Response
     */
    public function adminLoginAction(Request $request)
    {
        $session = $request->getSession();
 
        // get the login error if there is one
        if ($request->attributes->has(self::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(
                self::AUTHENTICATION_ERROR
            );
        } else {
            $error = $session->get(self::AUTHENTICATION_ERROR);
            $session->remove(self::AUTHENTICATION_ERROR);
        }
        
 
        return $this->render('@challengeUser/Security/login.html.twig',
            array(
                // last username entered by the user
                'last_username' => $session->get(self::LAST_USERNAME),
                'error'         => $error,
                // ...
            )
        );
    }

}
